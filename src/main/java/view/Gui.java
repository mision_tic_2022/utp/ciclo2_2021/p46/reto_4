package view;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;

import controller.Controlador;
import model.vo.Lider;

import java.awt.*;
import java.sql.SQLException;
import java.util.ArrayList;

public class Gui extends JFrame{
    //Atributos
    private DefaultTableModel model;
    private JTable table;
    private Controlador controlador;

    //Constructor
    public Gui(){
        controlador = new Controlador();
        //COnfiguración de la ventana
        this.setTitle("Reto 5");
        //Ubicación y tamaño de la ventana
        this.setBounds(0,0, 500, 500);
        this.setDefaultCloseOperation(EXIT_ON_CLOSE);
        //Indicar el tipo de layout a la ventana
        this.getContentPane().setLayout(new BorderLayout() );

        model = new DefaultTableModel();
        table = new JTable(model);

        model.addColumn("Nombre");
        model.addColumn("Apellido");

        //Panel para la tabla
        JPanel panel = new JPanel();
        panel.add( new JScrollPane(table) );

        vista_requerimiento_4();

        //añadimos elementos a la ventana
        this.add(panel, BorderLayout.CENTER);

    }


    public void vista_requerimiento_4() {
        try {

            ArrayList<Lider> lideres = controlador.Solucionar_requerimiento_4();
            for (int i = 0; i < lideres.size(); i++) {
                Object[] row = {lideres.get(i).getNombre(), lideres.get(i).getApellido()};
                //añadirlo a la tabla
                model.addRow(row);
            }

        } catch (SQLException e) {
            System.err.println("Ha ocurrido un error!" + e.getMessage());
        }

    }
    
    
}
