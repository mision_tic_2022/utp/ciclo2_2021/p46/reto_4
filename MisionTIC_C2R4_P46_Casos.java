//Librerías
import java.util.ArrayList;
import java.io.File;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;


public class Lider{
    /*************
     * Atributos
     *************/
    private String nombre;
    private String apellido;

    //Constructor
    public Lider(String nombre, String apellido) {
        this.nombre = nombre;
        this.apellido = apellido;
    }

    //Consultores y Modificadores
    
    public String getNombre() {
        return nombre;
    }
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    public String getApellido() {
        return apellido;
    }
    public void setApellido(String apellido) {
        this.apellido = apellido;
    }
 
}



public class Proyecto {

    private String fecha_inicio;
    private int num_habitaciones;
    private int num_banios;
    private String nombre_constructora;
    private int estrato_proyecto;
    private Lider lider;

    public Proyecto() {

    }

    /*******************************
     * Consultores y modificadores
     ********************************/

    public String getFecha_inicio() {
        return fecha_inicio;
    }

    public void setFecha_inicio(String fecha_inicio) {
        this.fecha_inicio = fecha_inicio;
    }

    public int getNum_habitaciones() {
        return num_habitaciones;
    }

    public void setNum_habitaciones(int num_habitaciones) {
        this.num_habitaciones = num_habitaciones;
    }

    public int getNum_banios() {
        return num_banios;
    }

    public void setNum_banios(int num_banios) {
        this.num_banios = num_banios;
    }

    public String getNombre_constructora() {
        return nombre_constructora;
    }

    public void setNombre_constructora(String nombre_constructora) {
        this.nombre_constructora = nombre_constructora;
    }

    public Lider getLider() {
        return lider;
    }

    public void setLider(Lider lider) {
        this.lider = lider;
    }

    public int getEstrato_proyecto() {
        return estrato_proyecto;
    }

    public void setEstrato_proyecto(int estrato_proyecto) {
        this.estrato_proyecto = estrato_proyecto;
    }

}



public class LiderDao {

    public ArrayList<Lider> query_requerimiento_4() throws SQLException {
        Connection conexion = JDBCUtilities.getConnection();
        // Crea arreglo para almacenar objetos tipo Proyecto
        ArrayList<Lider> lideres = new ArrayList<Lider>();
        // Consultas
        try {            
            // Ejecuta el query
            ResultSet query = conexion.createStatement().executeQuery(
                    "SELECT l.Nombre, l.Primer_Apellido FROM Proyecto p INNER JOIN Lider l ON l.ID_Lider = p.ID_Lider WHERE p.Constructora = 'Pegaso'");
            // Recorre los resultados del query
            while (query.next()) {
                // Almacena los resultados del query en un objeto Proyecto
                Lider objLider = new Lider(query.getString("Nombre"), query.getString("Primer_Apellido"));
                // agrega el objeto al arraylist
                lideres.add(objLider);
            }
        } catch (Exception e) {
            // TODO: handle exception
            System.out.println(e);
        }

        return lideres;
    }// Fin del método query_requerimiento_4

}



public class ProyectoDao {

    public ArrayList<Proyecto> query_requerimiento_1() throws SQLException {

        ArrayList<Proyecto> proyectos = new ArrayList<Proyecto>();
        Connection conexion = JDBCUtilities.getConnection();

        try {
            String consulta = "SELECT Fecha_Inicio, Numero_Habitaciones, Numero_Banos from Proyecto WHERE Constructora = 'Pegaso'";

            PreparedStatement statement = conexion.prepareStatement(consulta);
            ResultSet resultSet = statement.executeQuery();

            // Recorre los resultados del query
            while (resultSet.next()) {
                // Almacena los resultados del query en un objeto Proyecto
                Proyecto objProyecto = new Proyecto();
                objProyecto.setFecha_inicio(resultSet.getString("Fecha_Inicio"));
                objProyecto.setNum_habitaciones(resultSet.getInt("Numero_Habitaciones"));
                objProyecto.setNum_banios(resultSet.getInt("Numero_Banos"));
                // añadir el objeto al array
                proyectos.add(objProyecto);
            }

            resultSet.close();
            statement.close();

        } catch (SQLException e) {
            System.err.println("Error en la consulta " + e);
        } finally {
            if (conexion != null) {
                conexion.close();
            }
        }

        // Retornar la colección de vo's
        return proyectos;

    }

    public ArrayList<Proyecto> query_requerimiento_2() throws SQLException {
        Connection conexion = JDBCUtilities.getConnection();
        // Crea arreglo para almacenar objetos tipo Proyecto
        ArrayList<Proyecto> proyectos = new ArrayList<Proyecto>();
        // Consultas
        try {
            // Ejecuta el query
            ResultSet query = conexion.createStatement().executeQuery(
                    "SELECT p.Fecha_Inicio, p.Numero_Habitaciones, p.Numero_Banos, l.Nombre, l.Primer_Apellido, t.Estrato FROM Proyecto p INNER JOIN Lider l ON l.ID_Lider = p.ID_Lider INNER JOIN Tipo t ON t.ID_Tipo = p.ID_Tipo WHERE Constructora = 'Pegaso'  LIMIT 50");
            // Recorre los resultados del query
            while (query.next()) {
                // Almacena los resultados del query en un objeto Proyecto
                Proyecto objProyecto = new Proyecto();
                objProyecto.setFecha_inicio(query.getString("Fecha_Inicio"));
                objProyecto.setNum_habitaciones(query.getInt("Numero_Habitaciones"));
                objProyecto.setNum_banios(query.getInt("Numero_Banos"));
                objProyecto.setEstrato_proyecto(query.getInt("Estrato"));
                // Crea objeto Lider
                String nombre_lider = query.getString("Nombre");
                String apellido_lider = query.getString("Primer_Apellido");
                Lider objLider = new Lider(nombre_lider, apellido_lider);
                objProyecto.setLider(objLider);
                // añadir el objeto al array
                proyectos.add(objProyecto);
            }
        } catch (Exception e) {
            // TODO: handle exception
            System.out.println(e);
        }

        return proyectos;
    }// Fin del método query_requerimiento_2


    public ArrayList<Proyecto> query_requerimiento_3() throws SQLException {
        Connection conexion = JDBCUtilities.getConnection();
        // Crea arreglo para almacenar objetos tipo Proyecto
        ArrayList<Proyecto> proyectos = new ArrayList<Proyecto>();
        // Consultas
        try {            
            // Ejecuta el query
            ResultSet query = conexion.createStatement().executeQuery(
                    "SELECT SUM(Numero_Habitaciones) as Total_Habitaciones, Constructora FROM Proyecto p GROUP BY Constructora");
            // Recorre los resultados del query
            while (query.next()) {
                // Almacena los resultados del query en un objeto Proyecto
                Proyecto objProyecto = new Proyecto();
                objProyecto.setNum_habitaciones(query.getInt("Total_Habitaciones"));
                objProyecto.setNombre_constructora(query.getString("Constructora"));
                // añadir el objeto al array
                proyectos.add(objProyecto);
            }
        } catch (Exception e) {
            // TODO: handle exception
            System.out.println(e);
        }

        return proyectos;
    }// Fin del método query_requerimiento_3


    public ArrayList<Proyecto> query_requerimiento_5() throws SQLException{
        Connection conexion = JDBCUtilities.getConnection();
        // Crea arreglo para almacenar objetos tipo Proyecto
        ArrayList<Proyecto> proyectos = new ArrayList<Proyecto>();
        // Consultas
        try {            
            // Ejecuta el query
            ResultSet query = conexion.createStatement().executeQuery(
                    "SELECT SUM(Numero_Habitaciones) as Total_Habitaciones, Constructora FROM Proyecto p GROUP BY Constructora HAVING Total_Habitaciones > 200.0 ORDER BY Total_Habitaciones");
            // Recorre los resultados del query
            while (query.next()) {
                // Almacena los resultados del query en un objeto Proyecto
                Proyecto objProyecto = new Proyecto();
                objProyecto.setNum_habitaciones(query.getInt("Total_Habitaciones"));
                objProyecto.setNombre_constructora(query.getString("Constructora"));
                // añadir el objeto al array
                proyectos.add(objProyecto);
            }
        } catch (Exception e) {
            // TODO: handle exception
            System.out.println(e);
        }

        return proyectos;
    }// Fin del método query_requerimiento_4

}



public class Controlador {

    private final ProyectoDao proyectoDao;
    private final LiderDao liderDao;

    public Controlador() {
        this.proyectoDao = new ProyectoDao();
        this.liderDao = new LiderDao();
    }


    public ArrayList<Proyecto> Solucionar_requerimiento_1() throws SQLException {
        return this.proyectoDao.query_requerimiento_1();
    }

    public ArrayList<Proyecto> Solucionar_requerimiento_2() throws SQLException {
        return this.proyectoDao.query_requerimiento_2();
    }

    public ArrayList<Proyecto> Solucionar_requerimiento_3() throws SQLException {
        return this.proyectoDao.query_requerimiento_3();
    }

    public ArrayList<Lider> Solucionar_requerimiento_4() throws SQLException {
        return this.liderDao.query_requerimiento_4();
    }

    public ArrayList<Proyecto> Solucionar_requerimiento_5() throws SQLException {
        return this.proyectoDao.query_requerimiento_5();
    }

}



public class JDBCUtilities {

    // Atributos de clase para gestión de conexión con la base de datos
    private static final String UBICACION_BD = "ProyectosConstruccion.db";

    public static Connection getConnection() throws SQLException {
        String url = "jdbc:sqlite:" + UBICACION_BD;
        return DriverManager.getConnection(url);
    }

    public static boolean estaVacia() {
        File archivo = new File(JDBCUtilities.UBICACION_BD);
        return archivo.length() == 0;
    }

}



public class Vista {

    public static final Controlador controlador = new Controlador();

    public static void vista_requerimiento_1() {

        try {

            ArrayList<Proyecto> proyectos = controlador.Solucionar_requerimiento_1();
            for (int i = 0; i < proyectos.size(); i++) {
                String info = "Fecha_Inicio: " + proyectos.get(i).getFecha_inicio();
                info += " - Numero_Habitaciones: " + proyectos.get(i).getNum_habitaciones();
                info += " - Numero_Banos: " + proyectos.get(i).getNum_banios();
                System.out.println(info);
            }

        } catch (SQLException e) {
            System.err.println("Ha ocurrido un error!" + e.getMessage());
        }

    }

    public static void vista_requerimiento_2() {
        try {

            ArrayList<Proyecto> proyectos = controlador.Solucionar_requerimiento_2();
            for (int i = 0; i < proyectos.size(); i++) {
                String info = "Fecha_Inicio: " + proyectos.get(i).getFecha_inicio();
                info += " - Numero_Habitaciones: " + proyectos.get(i).getNum_habitaciones();
                info += " - Numero_Banos: " + proyectos.get(i).getNum_banios();
                info += " - Nombre_Lider: " + proyectos.get(i).getLider().getNombre();
                info += " - Apellido_Lider: " + proyectos.get(i).getLider().getApellido();
                info += " - Estrato_Proyecto: " + proyectos.get(i).getEstrato_proyecto();
                System.out.println(info);
            }

        } catch (SQLException e) {
            System.err.println("Ha ocurrido un error!" + e.getMessage());
        }

    }

    public static void vista_requerimiento_3() {
        try {

            ArrayList<Proyecto> proyectos = controlador.Solucionar_requerimiento_3();
            for (int i = 0; i < proyectos.size(); i++) {
                String info = "Total_Habitaciones: " + proyectos.get(i).getNum_habitaciones();
                info += " - Constructora: " + proyectos.get(i).getNombre_constructora();
                System.out.println(info);
            }

        } catch (SQLException e) {
            System.err.println("Ha ocurrido un error!" + e.getMessage());
        }

    }

    public static void vista_requerimiento_4() {
        try {

            ArrayList<Lider> lideres = controlador.Solucionar_requerimiento_4();
            for (int i = 0; i < lideres.size(); i++) {
                String info = "Nombre_Lider: " + lideres.get(i).getNombre() + " - Apellido_Lider: "
                        + lideres.get(i).getApellido();
                System.out.println(info);
            }

        } catch (SQLException e) {
            System.err.println("Ha ocurrido un error!" + e.getMessage());
        }

    }

    public static void vista_requerimiento_5() {
        try {

            ArrayList<Proyecto> proyectos = controlador.Solucionar_requerimiento_5();
            for (int i = 0; i < proyectos.size(); i++) {
                String info = "Total_Habitaciones: " + proyectos.get(i).getNum_habitaciones();
                info += " - Constructora: " + proyectos.get(i).getNombre_constructora();
                System.out.println(info);
            }

        } catch (SQLException e) {
            System.err.println("Ha ocurrido un error!" + e.getMessage());
        }

    }

}
